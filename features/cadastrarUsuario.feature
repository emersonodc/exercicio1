#language: pt
#utf-8

Funcionalidade: Cadastro de usuario

Eu como administrador
 Quero acessar o site ORANGEHRM
 Para cadastrar um novo funcionario
@cadastrar
Cenario: Cadastrar Usuário
 Dado que eu esteja logado no site 
 Quando preencher as informacoes necessarias
 Entao sera cadastrado um novo funcionario 