#language: pt
#utf-8

Funcionalidade: Alteracao de dados de funcionario

Eu como administrador
Quero alterar dados de um funcionario
Para que eles fiquem atualizados 
@alterar
Cenario: Alterar dados de Funcionario
Dado que eu esteja logado no site 
Quando alterar os dados de um funcionario
Entao eles serao atualizados