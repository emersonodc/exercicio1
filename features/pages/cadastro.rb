class Cadastro < SitePrism::Page

    set_url "http://opensource.demo.orangehrmlive.com/"

    element :usuario, "#txtUsername"
    element :senha, "#txtPassword"
    element :botao, "#btnLogin"
    element :acesso1, "#menu_pim_viewPimModule"
    element :acesso2, "#menu_pim_addEmployee" 
    element :add, "#btnAdd"   
    element :nome, "#firstName"
    element :sobrenome, "#lastName"
    element :salvar, "#btnSave"
    element :perfil, "#profile-pic"
        
    
    def login (paran1,paran2)
            usuario.set(paran1)
            senha.set(paran2)
            botao.click

    end

    def dados (vlr1,vlr2)
        nome.set(vlr1)
        sobrenome.set(vlr2)
        salvar.click
    end


    
end
