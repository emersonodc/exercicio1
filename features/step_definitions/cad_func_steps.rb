Dado("que eu esteja logado no site") do
    Cadastro.new.load
    Cadastro.new.login("Admin","admin")
    Cadastro.new.acesso1.click
    Cadastro.new.acesso2.click
end
  
  Quando("preencher as informacoes necessarias") do
   Cadastro.new.dados("Maria","Aparecida")
end
  
  Entao("sera cadastrado um novo funcionario") do
    Cadastro.new.perfil.assert_text('Maria Aparecida')
end
  